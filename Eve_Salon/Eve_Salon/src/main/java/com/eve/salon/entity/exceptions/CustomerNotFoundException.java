package com.eve.salon.entity.exceptions;

import org.springframework.http.HttpStatus;

public class CustomerNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public CustomerNotFoundException(String message) {
		super(message);
	}


}